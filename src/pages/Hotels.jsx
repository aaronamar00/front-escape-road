import React from "react";
//import { Link } from "react-router-dom";
// import image from "../design/banner.png";
// import products from "../__mocks__/data.json";
// import marketdata from "../__mocks__/aboutData";
import World_Maps from "../components/World_Maps";
import BookNow from "../components/BookNow";
import BookHome from "../components/Book_Home";
// import Search_All from "../components/Search_All"
import CardCarousel from '../components/Card_Suggestion'

const Products = () => {
  return (
    <div className="home">
      {/* <div className="home__banner">
        <Herobanner image={image} title={slogan} />
      </div>{" "} */}
      {/* <Search_All/> */}
      <CardCarousel/>
      <World_Maps/>
      <BookHome/>
      <BookNow/>
    </div>
  );
};

export default Products;
