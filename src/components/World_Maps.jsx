import React from 'react';
import mapsworld from "../design/maps_world.jpg";

const World_Maps = () => {
    return (
        <div>
        <img src={mapsworld} alt="" className="maps__world" />
        </div>
    );
};

export default World_Maps;
