import React, { useState } from 'react';
import Rating from './Rating'

const reviews = [
  {
    id: 1,
    client: 'Enzo Scaduto',
    travel: 'Voyage Professionel',
    description: 'Escape Road Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac dui quis velit faucibus lobortis. Vestibulum ante ipsum. Donec ac dui quis velit faucibus lobortis. Vestibulum ante ipsum',
    rating: 4,
  },
  {
    id: 2,
    client: 'Bob',
    travel: 'XYZ Corp.',
    description: 'Très satisfait de mon achat.',
    rating: 5,
  },
  {
    id: 3,
    client: 'Charlie',
    travel: '123 Co.',
    description: 'Rapide et efficace, rien à redire.',
    rating: 3,
  },
];

const Carousel = () => {
  const [currentReview, setCurrentReview] = useState(0);

  const handlePrev = () => {
    setCurrentReview(currentReview === 0 ? reviews.length - 1 : currentReview - 1);
  };

  const handleNext = () => {
    setCurrentReview(currentReview === reviews.length - 1 ? 0 : currentReview + 1);
  };

  return (
    <div className="carousel-testimonials">
      <div className="carousel__content">
        <p className="carousel__description"><i>"{reviews[currentReview].description}"</i></p>
        <Rating rating={reviews[currentReview].rating} />
        <p className="carousel__client">{reviews[currentReview].client}</p>
        <p className="carousel__typetravel">{reviews[currentReview].travel}</p>
      </div>
      <div className='carousel__button__group'>
        <button className="carousel__button carousel__button--prev" onClick={handlePrev}>
          &lt;
        </button>
        <button className="carousel__button carousel__button--next" onClick={handleNext}>
          &gt;
        </button>
      </div>
    </div>
  );
};

export default Carousel;