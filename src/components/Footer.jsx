import React from "react";
import logo from "../design/logo.svg";

function Footer() {
  const effectiveYear = new Date().getFullYear();

  return (
    <footer className="footer">
      <div class="footer">
  <div class="inner-footer">

    <div class="footer-items">
    <img src={logo} alt="" className="footer__logo" />
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eu ultrices arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eu ultrices arcu.</p>
    </div>

    <div class="footer-items">
      <h3>Quick Links</h3>
        <ul>
          <a href="#"><li>About us</li></a>
          <a href="#"><li>Features</li></a>
          <a href="#"><li>Contact</li></a>
        </ul>
    </div>
    
    <div class="footer-items">
      <h3>Quick Links</h3>
        <ul>
          <a href="#"><li>About us</li></a>
          <a href="#"><li>Features</li></a>
          <a href="#"><li>Contact</li></a>
        </ul>
    </div>

    <div class="footer-items">
      <h3>Quick Links</h3>
        <ul>
          <a href="#"><li>About us</li></a>
          <a href="#"><li>Features</li></a>
          <a href="#"><li>Contact</li></a>
        </ul>
    </div>
  </div>
  
    <div class="footer-bottom">
      Copyright &copy; Espace Road - All right reserved 
    </div>
  </div>
</footer>
  );
}

export default Footer;
