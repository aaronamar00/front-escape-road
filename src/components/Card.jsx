const Card = ({ image, city, country, rating, price }) => {
  return (
    <div className="card">
      <img src={image} alt="" />
      <h3>{city}, {country}</h3>
      <p>{rating}</p>
      <p>Price: {price}</p>
      <button>Reserve</button>
    </div>
  );
};

export default Card;