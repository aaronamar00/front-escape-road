import React from 'react';
import { Select, Space } from 'antd';

const Select_Nb_Passager = () => {
    return (
    <div>
        <Space wrap>
        <Select
        defaultValue="1 passager"
        style={{
            width: 140,
        }}
        bordered={false}
        options={[
            {
            value: '1 passager',
            label: '1 passager',
            },
            {
            value: '2 passagers',
            label: '2 passagers',
            },
            {
            value: '3 passagers',
            label: '3 passagers',
            },
        ]}
        />
        </Space>
    </div>
    );
};

export default Select_Nb_Passager;
