import React from 'react';
import { Button } from 'antd';

const BookNow = () => {
    return (
        <div className='BookNow'>
            <h1>Réservez dès maintenant avec</h1>
            <h3 className="text__orange">Escape Road</h3>
            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eu ultrices arcu.</h4>
            <Button className='button_book_now'>Réserver</Button>
        </div>
    );
};

export default BookNow;

