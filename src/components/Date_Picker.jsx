import { DatePicker, Space } from 'antd';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';

dayjs.extend(customParseFormat);
const { RangePicker } = DatePicker;
const disabledDate = (current) => {
  return current && current < dayjs().endOf('day');
};

const Date_Picker = () => (
  <Space direction="vertical" size={12}>
    <RangePicker disabledDate={disabledDate} />
  </Space>
);
export default Date_Picker;