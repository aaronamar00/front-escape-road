import React from 'react';
import { Select, Space } from 'antd';

const Select_Classe_avion = () => {
    return (
    <div>
        <Space wrap>
        <Select
        defaultValue="Classe économique"
        style={{
            width: 200,
        }}
        bordered={false}
        options={[
            {
            value: 'Classe économique',
            label: 'Classe économique',
            },
            {
            value: 'Classe affaire',
            label: 'Classe affaire',
            },
            {
            value: 'Classe première',
            label: 'Classe première',
            },
        ]}
        />
        </Space>
    </div>
    );
};

export default Select_Classe_avion;
