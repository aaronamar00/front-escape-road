// import { NavLink } from "react-router-dom";

//Style
// import Logo from "../design/logo.svg";
// import Login from "../design/login.svg";

// const StyledNavbar = () => {
//   return (
    // <nav className="navbar">
    //   <NavLink to="/">
    //     <img src={Logo} alt="Logo" className="navbar__img" />
    //   </NavLink>
    //   <div className="navbar__links">
    //     <NavLink
    //       to="/"
    //       className={({ isActive }) =>
    //         isActive ? "navbar__links--active" : "navbar__links--inactive"
    //       }
    //     >
    //       Vols
    //     </NavLink>

    //     <NavLink
    //       to="/hotels"
    //       className={({ isActive }) =>
    //         isActive ? "navbar__links--active" : "navbar__links--inactive"
    //       }
    //     >
    //       Hôtels
    //     </NavLink>

    //     <NavLink
    //       to="/bar"
    //       className={({ isActive }) =>
    //         isActive ? "navbar__links--active" : "navbar__links--inactive"
    //       }
    //     >
    //       Bar / Restaurant
    //     </NavLink>

    //     <NavLink
    //       to="/activites"
    //       className={({ isActive }) =>
    //         isActive ? "navbar__links--active" : "navbar__links--inactive"
    //       }
    //     >
    //       Activités
    //     </NavLink>

    //     <NavLink to="/">
    //     <img src={Login} alt="login" className="login__user" />
    //     </NavLink>

    //   </div>
    // </nav>
//   );
// };


// export default StyledNavbar;


import React from "react" 
import { useState } from "react"

function StyledNavbar() {
const [showLinks, setShowLinks] = useState(false)
const handleShowLinks = () => {
setShowLinks (!showLinks)
}

return(
<nav className={`navbar ${showLinks ? "show-nav" : "hide-nav"}`}>
<div className="navbar__ logo">Logo</div>
<ul className="navbar__links">
<li className="navbar__item">
<a href="/" className="navbar__link">
Accueil
</a>
</li>

<li className="navbar__item">
<a href="/" className="navbar__link">
Blog 
</a>
</li>

<li className="navbar__item">
<a href="/" className="navbar__link">
Portfolio
</a>
</li>
</ul>
<button className="navbar__burger" onClick={handleShowLinks}>
<span className="burger-bar"></span> 
</button>
</nav>
)
}

export default StyledNavbar;