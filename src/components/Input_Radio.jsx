import React from 'react';
import { Radio } from 'antd';
import { useState } from 'react';

const Input_Radio = () => {
const [value, setValue] = useState(1);
const onChange = (e) => {
console.log('radio checked', e.target.value);
      setValue(e.target.value);
};
    return (
        <div>
            <Radio.Group onChange={onChange} value={value}>
            <Radio value={1} className="choix_voyage_line1">Aller - Retour</Radio>
            <Radio value={2} className="choix_voyage_line1">Aller simple</Radio>
            </Radio.Group>
        </div>
    );
};

export default Input_Radio;
