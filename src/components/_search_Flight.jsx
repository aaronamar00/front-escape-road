import React from 'react';
import { UserOutlined } from '@ant-design/icons';
import { Input } from 'antd';
import Date_Picker from "./Date_Picker";
import Input_Radio from "./Input_Radio";
import Select_Classe_avion from "./Select_Classe_avion"
import flight_departure from "../design/Flight_Takeoff.svg";
import flight_arrival from "../design/Flight_Landing.svg";
import date_selector from "../design/Date_selector.svg";
import Select_Nb_Passager from "./Select_Nb_Passager"


const Search_Book = () => {
    return (
    <div className='Search_Book'>
        <div className='search_line'>
        <Input_Radio/>
        <Select_Nb_Passager/>
        <Select_Classe_avion/>
        </div>
        <div className='search_line'>
        <img src={flight_departure} alt="flight_departure" className="flight_img" /><Input size="large" placeholder="D'ou partez-vous ?" style={{ width: 200 }}/>
        <img src={flight_arrival} alt="flight_arrival" className="flight_img" /><Input size="large" placeholder="Où allez-vous ?" style={{ width: 200 }}/>
        <img src={date_selector} alt="date_selector" className="flight_img" /><Date_Picker/>
        </div>
    </div>
);
};

export default Search_Book;