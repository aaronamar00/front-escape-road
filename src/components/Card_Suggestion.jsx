import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import orangeStar from "../design/orangeStar.svg";

const CardCarousel = () => {
  // Données de cartes statiques pour l'exemple
  const cards = [
    {
      id: 1,
      image: 'http://127.0.0.1:5500/src/design/maldives.jpg',
      city: 'Maldives',
      country: 'Ouest, Etats Unies',
      rating: 4.7,
      price: '2499',
    },
    {
      id: 2,
      image: 'http://127.0.0.1:5500/src/design/newyork.jpg',
      city: 'New York',
      country: 'USA',
      rating: 4.8,
      price: '1700',
    },
    {
      id: 3,
      image: 'http://127.0.0.1:5500/src/design/chine.jpg',
      city: 'Tokyo',
      country: 'Japan',
      rating: 4.6,
      price: '1900',
    },
  ];

  const renderCard = (card) => {
    return (
      <div key={card.id} className="card">
        <div>
        <img src={card.image} alt={card.city}/>
        </div>
        <div className="global">
          <div className="div1">
            <h2>{card.city}</h2>
            <h4>{card.country}</h4>
          </div> 
          <div className="div2">
            <img src={orangeStar} className='rating_suggestions'/>{card.rating}
          </div> 
        </div>
        
        <div className="global"> 
        <div className="div3">
          <h1><span className="text__orange">€</span>{card.price}<span className='personne'>/personne</span></h1>
        </div> 
        <div className="div4">
          <button>Réserver</button>
        </div> 
        </div>
      </div>

      // <div key={card.id} className="card">
      //     
      //     <h4>{card.country}</h4>
      //   <div className='card_inside'>
      //     <img src={orangeStar} className='rating_suggestions'/>{card.rating}
      //   </div>
      //   <h1><span className="text__orange">€</span>{card.price}<span className='personne'>/personne</span></h1>
      //   <button>Réserver</button>
      // </div>
    );
  };

  return (
    <div className='Suggestion_Destinations'>
    <h1>Découvrez les <span className="text__orange">destinations</span> <br /> tendances en ce moment</h1>
    <Carousel showArrows={true} showThumbs={true} showStatus={false} infiniteLoop={true} centerMode={true} centerSlidePercentage={33.33} swipeable={true} emulateTouch={true} dynamicHeight={false} selectedItem={1} axis={'horizontal'} interval={5000} transitionTime={500}>
      {[...cards, ...cards, ...cards].map((card) => renderCard(card))}
    </Carousel>
    </div>
  );
};

export default CardCarousel;