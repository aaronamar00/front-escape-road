import React from "react";
import BookImage from "../design/image_gauche.png";
import Carousel from "../components/Testimonials";

const BookHome = ({ image, text }) => {
  return (
      <div className="parent">
          <div className="divenfant">
            <h1>Réservez votre <span className="text__orange">voyage</span> dès maintenant</h1>
            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac dui quis velit faucibus lobortis. Vestibulum ante ipsum</h4>
            <Carousel />
          </div>
          <div className="divenfant">
            <img src={BookImage} alt="" className="book__img" />
          </div>
      </div>
  );
};

export default BookHome;