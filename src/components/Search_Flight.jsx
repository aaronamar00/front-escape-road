import React from 'react';
import Search_Book from "../components/_search_Flight";

const Search_Flight = () => {
    return (
        <div className='Search_All'>
            <Search_Book/>
            <button className='Rechercher'>Rechercher</button>
        </div>
    );
};

export default Search_Flight;